package ru.t1.kharitonova.tm.api.controller;

public interface ITaskController {
    void createTask();

    void showTasks();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();
}
